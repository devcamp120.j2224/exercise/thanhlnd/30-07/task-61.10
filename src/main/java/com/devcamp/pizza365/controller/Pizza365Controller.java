package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.model.Customer;
import com.devcamp.pizza365.model.Drink;
import com.devcamp.pizza365.model.Order;
import com.devcamp.pizza365.model.Voucher;
import com.devcamp.pizza365.repository.ICMenuRepository;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IDrinkRepository;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.iVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class Pizza365Controller {
    @Autowired
    IDrinkRepository pDrinkRepository;

    @Autowired
    iVoucherRepository pIVoucherRepository;

    @Autowired
    ICMenuRepository pIcMenuRepository;

    @Autowired
    ICustomerRepository pCustomerRepository;

    @Autowired
    IOrderRepository pIOrderRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrinks() {
        try {
            List<Drink> listDrink = new ArrayList<Drink>();
            pDrinkRepository.findAll().forEach(listDrink::add);
            return new ResponseEntity<List<Drink>>(listDrink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/vouchers")
    public ResponseEntity<List<Voucher>> getAllVouchers() {
        try {
            List<Voucher> listVoucher = new ArrayList<Voucher>();
            pIVoucherRepository.findAll().forEach(listVoucher::add);
            return new ResponseEntity<List<Voucher>>(listVoucher, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);        
        }
    }

    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getAllMenu() {
        try {
            List<CMenu> listMenu = new ArrayList<CMenu>();
            pIcMenuRepository.findAll().forEach(listMenu::add);
            return new ResponseEntity<List<CMenu>>(listMenu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);        
        }
    }

    @GetMapping("/customer")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            List<Customer> listCustomer = new ArrayList<Customer>();
            pCustomerRepository.findAll().forEach(listCustomer::add);
            return new ResponseEntity<List<Customer>>(listCustomer, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); 
        }
    }

    @GetMapping("/orders")
    public ResponseEntity<Set<Order>> getOrderList(@RequestParam (value = "orderId") Integer order){
        try{
            Customer vCustomer = pCustomerRepository.findById(order);
            if(vCustomer != null){
                return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
